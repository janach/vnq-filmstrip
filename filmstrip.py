from PIL import Image, ImageOps, ImageFont, ImageDraw
from os.path import join
import numpy as np


class Filmstrip(object):
    def __init__(self, width_cm, dpi, hspace_cm):
        self.width_cm = width_cm
        self.width_px = (width_cm / 2.54)*dpi
        self.dpi = dpi
        self.hspace_cm = hspace_cm
        self.hspace_px = (hspace_cm / 2.54)*dpi
        self.canvas = None
        
    def _create_canvas(self, width_px, height_px):
        size = (int(width_px), int(height_px))
        mode = 'RGBA'
        image = Image.new(mode, size)
        self.canvas = image
    
    def place_images(self, images):
        raw_img_width_px = sum(img.width for img in images)
        max_raw_img_height_px = max(img.height for img in images)
        raw_strip_width_px = self.width_px - (len(images) - 1) * self.hspace_px
        factor = raw_strip_width_px / raw_img_width_px
        max_img_height_px = max_raw_img_height_px * factor
        self._create_canvas(self.width_px, max_img_height_px)

        # resize images and place on canvas
        left_px = 0
        for image in images:
            img = image.resize((np.array(image.size)*factor).astype(np.int),
                               resample=Image.BILINEAR)
            self.canvas.paste(img, (int(left_px), int(0)))
            left_px += img.width + self.hspace_px

    @property
    def height_px(self):
        return self.canvas.height

    @property
    def height_cm(self):
        return (self.height_px / self.dpi)*2.54


def crop_image(image, crop_mode):
    # crop_box = (left, upper, right, lower)
    if crop_mode == 'ticks':
        crop_box = (68, 46, 571, 363)
    elif crop_mode == 'ticklabels':
        crop_box = (41, 46, 571, 374)
    elif crop_mode == 'labels':
        crop_box = (16, 46, 571, 392)
    else:
        raise ValueError('Invalid crop_mode')
    cropped_image = image.crop(crop_box)
    return cropped_image


def add_border(image, white_border_px, black_border_px):
    image = ImageOps.expand(image, border=white_border_px, fill='white')
    image = ImageOps.expand(image, border=black_border_px, fill='black')
    return image


def add_epoch_text(image, epoch):
    text = 'After epoch {}'.format(epoch+1)
    fontsize_px = 40
    margin_top_px = 5
    margin_bottom_px = 10
    text_h_px = fontsize_px + margin_bottom_px + margin_top_px
    image = ImageOps.expand(image, border=(0, text_h_px, 0, 0), fill='white')
    d = ImageDraw.Draw(image)
    font = ImageFont.truetype('Pillow/Tests/fonts/DejaVuSans.ttf', fontsize_px)
    text_w, _ = d.textsize(text, font)
    left = image.width/2 - text_w/2
    upper = margin_top_px
    d.text((left, upper), text, fill='black', font=font)
    return image


def main():
    crop_mode = 'ticks' # ticks, ticklabels, labels
    epoch_ids = [0, 4, 9, 14,] + list(np.arange(20, 201, 20)-1)
    img_dir = 'images'
    # load images
    images = []
    for epoch in epoch_ids:
        img = Image.open(join(img_dir, 'em_ep{}.png'.format(epoch)))
        img = crop_image(img, crop_mode)
        img = add_border(img, white_border_px=10, black_border_px=2)
        img = add_epoch_text(img, epoch)
        images.append(img)
    
    strip = Filmstrip(width_cm=110, dpi=600, hspace_cm=0.3)
    strip.place_images(images)
    print("Strip dimension: width {} cm, height {} cm".
          format(strip.width_cm, strip.height_cm))
    strip.canvas.save('strip_600dpi_bilinear.png', 'PNG')

if __name__ == "__main__":
    main()
